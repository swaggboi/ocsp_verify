# ocsp_verify

Ruby script to craft OCSP queries from AIA field of SSL cert(s)

PEMs for subject and issuer of https://www.swagg.net and
https://codeberg.org provided as examples

## Run it

### Install Gems

You shouldn't really need to this; all machines I've tested this on
include these gems alongside the standard Ruby package:

    $ bundle install

### Print usage help

    $ ./ocsp_verify.rb --help
    Usage: ocsp_verify.rb [OPTIONS] <ISSUER CERT> <SUBJECT CERT(S)>
        -h, --help                       Show this help message
        -i, --ignore-signature           Don't validate signatures
        -n, --nonce                      Use nonce (CA must support this)
        -r, --root=FILE                  Add root cert to trust chain

### Send it

    $ ./ocsp_verify.rb le.pem swagg.pem codeberg.pem
    swagg.pem (ipv6.swagg.net) is valid
    codeberg.pem (codeberg.org) is valid

## TODOs

1. Implement versioning
